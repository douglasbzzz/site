<?php
    $erro = isset($_GET['mensagem']) ? $_GET['mensagem'] : 3;
?>
<!DOCTYPE HTML>
<html lang="pt-br" >
<!--
	Developed by Porthos Info LTDA™.
	Copyright 2018 - All rights reserved.
	Highlights by HTML5 UP
	html5up.net
-->
<html>
	<head>
		<title>Porthos Info</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
    	<meta name="mobile-web-app-capable" content="yes">
		<link rel="shortcut icon" href="images/favicon.ico" />
		<meta name="msapplication-TileColor" content="#007B8E">
		<meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
		<meta name="theme-color" content="#007B8E">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		
		<!-- Header -->
			<section id="header" class="gradient">
				<header class="major">
					<h1><img class="logotop" src="images/pi1.png"/></h1>
					<p class="slogan">Somos movidos pela educação</p>
				</header>
				<div class="container">
					<ul class="actions">
						<li><a href="#one" class="button special scrolly">Veja mais</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="main special">
				<div class="container">
					<span class="image fit primary"><img src="images/235.jpg" alt="" /></span>
					<div class="content">
						<header class="major">
							<h2>Quem somos?</h2>
						</header>
						<p>Somos uma startup pato-branquense que nasceu para facilitar a troca de experiências entre educadores. Atualmente apostamos também na prestação de serviços estudantis, oferecendo soluções que conectem usuários que visem promover uma educação mais justa e com maior qualidade.</p>
					</div>
					<a href="#two" class="goto-next scrolly">Next</a>
				</div>
			</section>

		<!-- Two -->
				<section id="two" class="main special">
					<div class="container">
						<span class="image fit primary"><img src="images/aass.jpg" alt="" /></span>
						<div class="content">
							<header class="major">
								<h2>O que fazemos?</h2>
							</header>
							<p>Trabalhamos com tecnologias web, desenvolvendo sites ondemand e nossas próprias soluções: Porthos Learn e Porthos Review, que ainda estão em construção ;)</p>
							<ul class="icons-grid">
								<li>
									<span class="icon major fa-book"></span>
									<h3>porthos learn</h3>
								</li>
								<li>
									<span class="icon major fa-search"></span>
									<h3>porthos review</h3>
								</li>
								<li>
									<span class="icon major fa-code"></span>
									<h3>sites ondemand</h3>
								</li>
								<li>
									<span class="icon major fa-coffee"></span>
									<h3>café</h3>
								</li>
							</ul>
						</div>
						<a href="#three" class="goto-next scrolly">Next</a>
					</div>
				</section>

		<!-- Three -->
			<section id="three" class="main special">
				<div class="container">
					<span class="image fit primary"><img src="images/faz.jpg" alt="" /></span>
					<div class="content">
						<header class="major">
							<h2>Missão, Visão e Valores</h2>
						</header>
						<h5>Nossa Missão:</h5>
						<blockquote>Proporcionar crescimento pessoal e profissional aos usuários, desenvolvendo soluções tecnológicas inovadoras.
						</blockquote>
						
						<h5>Nossa Visão:</h5>
						<blockquote>Ser referência no ramo tecnológico, contribuindo para uma sociedade mais conectada, prezando pela qualidade e confiabilidade das soluções.
						</blockquote>
						
						<h5>Nossos Valores:</h5>
						<blockquote> 
							Qualidade nas soluções oferecidas;<br/>
							Satisfação dos usuários;<br/>
							Inovação constante;<br/>
							Confiabilidade;<br/>
							Ética e respeito;<br/>
						</blockquote>
					</div>
					<a href="#footer" class="goto-next scrolly">Next</a>
				</div>
			</section>


		<!-- Footer -->
			<section id="footer">
				<div class="container">
					<header class="major">
						<h2>Gostou? Contate-nos =)</h2>
					</header>
					<form method="post" action="classes/contactController.php">
						<div class="row uniform">
							<div class="6u 12u$(xsmall)"><input type="text" name="txtNome" id="txtNome" placeholder="Nome" required/></div>
							<div class="6u$ 12u$(xsmall)"><input type="email" name="txtEmail" id="txtEmail" placeholder="Email" required /></div>
							<div class="6u 12u$(xsmall)"><input type="text" name="txtFone" id="txtFone"  onkeyup="mascara(this,mtel);" maxlength="15" placeholder="Fone" required /></div>
							<div class="6u 12u$(xsmall)"><input type="text" name="txtCidade" id="txtCidade" placeholder="Cidade" required /></div>
							<div class="12u$"><textarea name="txtMensagem" id="txtMensagem" placeholder="Mensagem" rows="6"></textarea></div>
							<div class="12u$">
								<ul class="actions">
									<li><input type="submit" value="Enviar" class="special" /></li>
								</ul>
							</div>
                            <?php
                            if ($erro == 2) {
                                echo '<font color="#FF0000">Sua mensagem não foi enviada... tente novamente, por favor!<br/></font>';
                            }elseif($erro == 1){
                                echo '<font color="#008000">Sua mensagem foi enviada! Logo entraremos em contato...<br/></font>';
                            }
                            ?>
						</div>
					</form>
				</div>
				<footer class="rodape">
					<ul class="icons">
						<li><img class="logobottom" src="images/porthos2.png"/></li>
					</ul>
					<ul class="icons">
						<li><a href="https://www.facebook.com/porthosinfo/" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/porthosinfo/" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="mailto:contato@porthosinfo.com" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>Copyright &copy; 2018 </li><li>Feito com ♥ pela Porthos Info</li><li>Todos os direitos Reservados</li>
					</ul>
				</footer>
			</section>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			
			<!--formatação de telefone-->
			<script type="text/javascript">
				function mascara(o,f){
					v_obj=o
					v_fun=f
					setTimeout("execmascara()",1)
				}
				function execmascara(){
					v_obj.value=v_fun(v_obj.value)
				}
				function mtel(v){
					v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
					v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
					v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
					return v;
				}
			</script>

	</body>
</html>