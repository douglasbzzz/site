<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 02/06/2018
 * Time: 15:51
 */

require_once ("Crud.php");
class Contato extends Crud{
    protected $table = "contato";
    private $nome;
    private $email;
    private $telefone;
    private $cidade;
    private $mensagem;

    public function insert(){
        $sql = "insert into $this->table(nome, email, telefone, cidade, mensagem) VALUES(:nome, :email, :telefone, :cidade, :mensagem )";
        $stmt = DB::prepare($sql);
        $stmt ->bindParam(':nome',$this->nome);
        $stmt ->bindParam(':email',$this->email);
        $stmt ->bindParam(':telefone',$this->telefone);
        $stmt ->bindParam(':cidade',$this->cidade);
        $stmt ->bindParam(':mensagem',$this->mensagem);
        return $stmt->execute();
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param mixed $mensagem
     */
    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
    }


}