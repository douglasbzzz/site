<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 02/06/2018
 * Time: 15:43
 */
//dependencias
require_once ('Contato.php');
//require_once ('../SQL/Persist.php');
require_once ('funcoes.php');
//novos objetos
$oContato = new Contato();
//$oPersist = new Persist();

if(isset($_POST['txtNome']) && $_POST['txtNome'] <> ''){
    //$nome = addslashes($_POST['txtNome']);
    $oContato->setNome(addslashes($_POST['txtNome']));
}
if(isset($_POST['txtEmail']) && $_POST['txtEmail'] <> ''){
    $oContato->setEmail(addslashes($_POST['txtEmail']));
}
if(isset($_POST['txtFone']) && $_POST['txtFone'] <> ''){
    $oContato->setTelefone(addslashes($_POST['txtFone']));
}
if(isset($_POST['txtCidade']) && $_POST['txtCidade'] <> ''){
    $oContato->setCidade(addslashes($_POST['txtCidade']));
}
if(isset($_POST['txtMensagem']) && $_POST['txtMensagem'] <> ''){
    $oContato->setMensagem(addslashes($_POST['txtMensagem']));
}

try{

    if($oContato->insert()){
        header('Location: ../index.php?mensagem=1');
    }else{
        header('Location: ../index.php?mensagem=2');
    }
}catch (\Exception $e){
    echo $e->getMessage();
}



?>